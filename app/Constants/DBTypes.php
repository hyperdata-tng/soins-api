<?php


namespace App\Constants;


class DBTypes
{

    const menu = 'MENU';
    const menuWeb = 'MWEB';

    const role = 'ROLE';

    const menuAccess = 'MACC';
    const menuAccessView = 'MACCV';
    const menuAccessCreate = 'MACCA';
    const menuAccessUpdate = 'MACCU';
    const menuAccessDelete = 'MACCD';
}
