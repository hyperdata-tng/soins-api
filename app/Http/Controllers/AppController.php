<?php


namespace App\Http\Controllers;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Models\Masters\Menu;
use App\Models\Masters\User;
use App\Models\Masters\UserDetail;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;

class AppController extends  Controller
{

    /* @var Menu|Relation */
    protected $menu;

    /* @var Menu|Relation */
    protected $user;

    public function __construct()
    {
        $this->menu = new Menu();
        $this->user = new User();
    }

    public function update(Request $req)
    {
        try {

            $userid = $req->input('userid');
            $bpid = $req->input('bpid');
            $usertypeid = $req->input('usertypeid');

            $user = $this->user->withJoin($this->user->defaultSelects)
                ->with([
                    'userdetail' => function($query) {
                        UserDetail::foreignSelect($query)
                            ->addSelect('userid');
                    }
                ])
                ->find($userid);

            if(is_null($user))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $userdetail = null;
            foreach($user->userdetail as $usrdetail) {
                if($usrdetail->bpid == $bpid && $usrdetail->usertypeid == $usertypeid) {
                    $userdetail = $usrdetail;
                }
            }

            if(is_null($userdetail))
                $userdetail = $user->userdetail[0];

            return $this->jsonData(array(
                'user' => array(
                    'userid' => $user->userid,
                    'userfullname' => $user->userfullname,
                    'userdetail' => $userdetail,
                    'userdetails' => $user->userdetail,
                    'token' => str_replace('Bearer ', '', $req->header('Authorization')),
                    'expired' => date('Y-m-d H:i:s', strtotime('+'.env('JWT_TTL').' seconds')),
                ),
                'accessmenus' => $this->menu->getAccessMenu($usertypeid, $bpid),
            ));
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'update');
        }
    }
}
