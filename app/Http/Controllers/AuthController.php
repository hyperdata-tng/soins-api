<?php


namespace App\Http\Controllers;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Models\Masters\Menu;
use App\Models\Masters\User;
use App\Models\Masters\UserDetail;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    /* @var User|Relation */
    protected $user;

    /* @var Menu|Relation*/
    protected $menu;

    public function __construct()
    {
        $this->user = new User();
        $this->menu = new Menu();
    }

    public function login(Request $req)
    {
        try {

            $this->customValidate($req->all(), array(
                'username:Nama pengguna' => 'required|string',
                'password:Kata sandi' => 'required|string',
            ));

            $credentials = $req->only(['username', 'password']);

            if (! $token = Auth::attempt($credentials, true))
                throw new Exception(DBMessage::AUTH_LOGIN_FAILED, DBCode::AUTHORIZED_ERROR);

            /* @var User $user*/
            $user = \auth()->user();

            $datauser = $this->user->withJoin($this->user->defaultSelects)
                ->with([
                    'userdetail' => function($query) {
                        UserDetail::foreignSelect($query)
                            ->addSelect('userid');
                    }
                ])
                ->find($user->userid);

            $userdetail = $datauser->userdetail[0];

            $response['userid'] = $datauser->userid;
            $response['userfullname'] = $datauser->userfullname;
            $response['userdetail'] = $userdetail;
            $response['userdetails'] = $datauser->userdetail;
            $response['token'] = $token;
            $response['expired'] = date('Y-m-d H:i:s', strtotime('+'.env('JWT_TTL').' seconds'));

            return $this->jsonSuccess(null, $response);
        } catch (Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function check()
    {
        try {
            return $this->jsonData(\auth()->user());
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'check');
        }
    }
}
