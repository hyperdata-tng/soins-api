<?php


namespace App\Http\Controllers\Masters;


use App\Constants\DBCode;
use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Masters\Menu;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MenuController extends  Controller
{

    /* @var Menu|Relation */
    protected $menu;

    public function __construct()
    {
        $this->menu = new Menu();
    }

    public function access(Request $req)
    {
        try {
            $bpid = $req->input('bpid');
            $usertypeid = $req->input('usertypeid');

            return $this->jsonData($this->menu->getAccessMenu($usertypeid, $bpid));
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'access');
        }
    }

    public function select(Request $req)
    {
        try {
            $searchValue = trim(strtolower($req->searchValue));
            $query = $this->menu->withJoin($this->menu->defaultSelects)
                ->with([
                    'parent' => function($query) {
                        Menu::foreignSelect($query)
                            ->with([
                                'parent' => function($query) {
                                    Menu::foreignSelect($query);
                                }
                            ])
                            ->addSelect('masterid');
                    }
                ])
                ->addSelect('masterid')
                ->where(DB::raw('TRIM(LOWER(menunm))'), 'like', "%$searchValue%");

            return $this->jsonData($query->get());
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'selectApi');
        }
    }

    public function datatables()
    {
        try {
            $query = $this->menu->withJoin($this->menu->defaultSelects)
                ->with([
                    'parent' => function($query) {
                        Menu::foreignSelect($query);
                    }
                ])
                ->addSelect('masterid');

            return $this->jsonData(datatables()->eloquent($query)
                ->toJson()
                ->getOriginalContent()
            );
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'datatables');
        }
    }

    public function store(Request $req)
    {
        try {
            $this->menu->create($req->all());

            return $this->jsonSuccess(DBMessage::SUCCESS_ADD);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'store');
        }
    }

    public function show($id)
    {
        try {
            $row = $this->menu->withJoin($this->menu->defaultSelects)
                ->with([
                    'parent' => function($query) {
                    Menu::foreignSelect($query)
                        ->with([
                            'parent' => function($query) {
                                Menu::foreignSelect($query);
                            }
                        ])
                        ->addSelect('masterid');
                }
                ])
                ->addSelect('masterid')
                ->find($id);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            return $this->jsonData($row);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'show');
        }
    }

    public function update(Request $req, $id)
    {
        try {
            $row = $this->menu->find($id, ['menuid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->update($req->all());

            return $this->jsonSuccess(DBMessage::SUCCESS_EDIT);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'update');
        }
    }

    public function destroy($id)
    {
        try {
            $row = $this->menu->find($id, ['menuid']);

            if(is_null($row))
                throw new Exception(DBMessage::ERROR_CORRUPT_DATA, DBCode::AUTHORIZED_ERROR);

            $row->delete();

            return $this->jsonSuccess(DBMessage::SUCCESS_DELETED);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'destroy');
        }
    }
}
