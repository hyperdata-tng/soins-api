<?php


namespace App\Http\Controllers\Masters;


use App\Http\Controllers\Controller;
use App\Models\Masters\BusinessPartner;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BusinessPartnerController extends  Controller
{

    /* @var BusinessPartner|Relation */
    protected $businesspartner;

    public function __construct()
    {
        $this->businesspartner = new BusinessPartner();
    }

    public function select(Request $req)
    {
        try {
            $searchValue = trim(strtolower($req->searchValue));
            $query = $this->businesspartner->withJoin($this->businesspartner->defaultSelects)
                ->where(DB::raw('TRIM(LOWER(bpname))'), 'like', "%$searchValue%");

            $json = array();
            foreach($query->get() as $db)
                $json[] = ['value' => $db->bpid, 'text' => $db->bpname];

            return $this->jsonData($json);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'select');
        }
    }
}
