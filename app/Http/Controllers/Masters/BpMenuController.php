<?php


namespace App\Http\Controllers\Masters;


use App\Constants\DBMessage;
use App\Http\Controllers\Controller;
use App\Models\Masters\BusinessPartnerMenu;
use App\Models\Masters\Menu;
use App\Models\Masters\Role;
use Exception;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BpMenuController extends Controller
{

    /* @var BusinessPartnerMenu|Relation */
    protected $bpmenu;

    /* @var Menu|Relation */
    protected $menu;

    /* @var Role|Relation */
    protected $role;

    public function __construct()
    {
        $this->menu = new Menu();
        $this->role = new Role();
        $this->bpmenu = new BusinessPartnerMenu();
    }

    public function load(Request $req)
    {
        try {
            $bpid = $req->input('bpid');
            $menutypeid = $req->input('menutypeid');

            $bpmenus = $this->bpmenu->withJoin($this->bpmenu->defaultSelects)
                ->with([
                    'menu' => function($query) {
                        Menu::foreignSelect($query)
                            ->addSelect('masterid');
                    }
                ])
                ->where('bpid', $bpid)
                ->whereHas('menu', function($query) use ($menutypeid) {
                    /* @var Relation $query */
                    $query->where('menutypeid', $menutypeid);
                })
                ->orderBy('menuid')
                ->get();

            $menus = $this->menu->withJoin($this->menu->defaultSelects)
                ->where('menutypeid', $menutypeid)
                ->addSelect('masterid')
                ->get();

            foreach($menus as $menu) {
                foreach($bpmenus as $bpmenu) {
                    if($menu->menuid == $bpmenu->menuid) {
                        $menu->checked = true;
                        $menu->bpmenuid = $bpmenu->bpmenuid;
                    }
                }
            }

            return $this->jsonData($menus);
        } catch (Exception $e) {
            return $this->jsonError($e, __CLASS__, 'load');
        }
    }

    public function save(Request $req)
    {
        try {
            $bpid = $req->input('bpid');
            $userid = $req->input('userid');

            DB::beginTransaction();

            $menus = json_decode($req->input('menus'));
            foreach($menus as $menu) {
                if($menu->checked == 'true' && $menu->bpmenuid == 0) {
                    $this->bpmenu->create([
                        'bpid' => $bpid,
                        'menuid' => $menu->menuid,
                        'createdby' => $userid,
                        'updatedby' => $userid,
                    ]);
                }

                else if($menu->checked == 'false' && $menu->bpmenuid != 0) {
                    $row = $this->bpmenu->find($menu->bpmenuid, ['bpmenuid']);
                    if(!is_null($row)) {

                        $this->role->where('bpid', $bpid)
                            ->where('menuid', $menu->menuid)
                            ->delete();

                        $row->delete();
                    }
                }
            }

            DB::commit();

            return $this->jsonSuccess(DBMessage::SUCCESS);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonError($e, __CLASS__, 'save');
        }
    }
}
