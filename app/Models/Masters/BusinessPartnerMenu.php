<?php


namespace App\Models\Masters;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class BusinessPartnerMenu extends  Model
{

    protected $table = "stbpmenu";
    protected $primaryKey = "bpmenuid";

    protected $fillable = [
        'bpid',
        'menuid',
        'createdby',
        'updatedby',
        'isactive',
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array();

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $bpMenu = new BusinessPartnerMenu();
        return $bpMenu->withJoin(is_null($selects) ? $bpMenu->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|BusinessPartnerMenu $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
            'menu' => function($query) {
                Menu::foreignSelect($query);
            }
        ])->select('bpmenuid', 'menuid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|BusinessPartnerMenu
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function menu()
    {
        return $this->hasOne(Menu::class, 'menuid', 'menuid');
    }

    public function access()
    {
        return $this->hasMany(Role::class, 'menuid', 'menuid');
    }

    public function businesspartner()
    {
        return $this->hasOne(BusinessPartner::class, 'bpid', 'bpid');
    }
}
