<?php


namespace App\Models\Masters;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Builder;

class Role extends Model
{

    protected $table = "msrole";
    protected $primaryKey = "roleid";

    protected $fillable = [
        "usertypeid",
        "menuid",
        "bpid",
        "accessid",
        "createdby",
        "updatedby"
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array();

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $role = new Role();
        return $role->withJoin(is_null($selects) ? $role->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|Role $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
            'access' => function($query) {
                Types::foreignSelect($query);
            }
        ])->select('roleid', 'accessid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|Role
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function usertype()
    {
        return $this->hasOne(Types::class, 'typeid', 'usertypeid');
    }

    public function menu()
    {
        return $this->hasOne(Menu::class, 'menuid', 'menuid');
    }

    public function bussinespartner()
    {
        return $this->hasOne(BusinessPartner::class, 'bpid', 'bpid');
    }

    public function access()
    {
        return $this->hasOne(Types::class, 'typeid', 'accessid');
    }

    /**
     * @param int $usertypeid
     * @param int $bpid
     * @return Relation|Builder
     * */
    public function role($usertypeid, $bpid)
    {
        return $this->withJoin($this->defaultSelects)
            ->with([
                'menu' => function($query) {
                    Menu::foreignSelect($query);
                },
            ])
            ->addSelect('menuid')
            ->where('usertypeid', $usertypeid)
            ->where('bpid', $bpid);
    }
}
