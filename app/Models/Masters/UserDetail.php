<?php


namespace App\Models\Masters;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class UserDetail extends  Model
{

    protected $table = "msuserdt";
    protected $primaryKey = "userdtid";

    protected $fillable = [
        "userid",
        "usertypeid",
        "bpid",
        "referalcode",
        "createdby",
        "updatedby",
        "isactive"
    ];

    const CREATED_AT = "createddate";
    const UPDATED_AT = "updateddate";

    public $defaultSelects = array();

    /**
     * @param Relation $query
     * @param array|null $selects
     * @return Relation
     * */
    static public function foreignSelect($query, $selects = null)
    {
        $userdt = new UserDetail();
        return $userdt->withJoin(is_null($selects) ? $userdt->defaultSelects : $selects, $query);
    }

    /**
     * @param Relation|UserDetail $query
     * @param array $selects
     * @return Relation
     * */
    private function _withJoin($query, $selects = array())
    {
        return $query->with([
            'businesspartner' => function($query) {
                BusinessPartner::foreignSelect($query);
            },
            'usertype' => function($query) {
                Types::foreignSelect($query);
            }
        ])->select('userdtid', 'bpid', 'usertypeid')->addSelect($selects);
    }

    /**
     * @param array $selects
     * @param Relation|UserDetail
     * @return Relation
     * */
    public function withJoin($selects = array(), $query = null)
    {
        return $this->_withJoin(is_null($query) ? $this : $query, $selects);
    }

    public function businesspartner()
    {
        return $this->hasOne(BusinessPartner::class, 'bpid', 'bpid');
    }

    public function usertype()
    {
        return $this->hasOne(Types::class, 'typeid', 'usertypeid');
    }
}
