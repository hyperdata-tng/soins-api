<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'auth'], function() use ($router) {

    $router->post('login', ['uses' => 'AuthController@login']);
    $router->post('check', ['middleware' => 'auth', 'uses' => 'AuthController@check']);
});

$router->group(['middleware' => 'auth'], function() use ($router) {
    $router->get('update', ['uses' => 'AppController@update']);

    $router->group(['namespace' => 'Masters'], function() use ($router) {

        $router->group(['prefix' => 'types'], function() use ($router) {

            $router->get('fetch', ['uses' => 'TypesController@fetch']);
            $router->get('select', ['uses' => 'TypesController@select']);
            $router->post('datatables', ['uses' => 'TypesController@datatables']);

            $router->post('', ['uses' => 'TypesController@store']);
            $router->get('{id}', ['uses' => 'TypesController@show']);
            $router->put('{id}', ['uses' => 'TypesController@update']);
            $router->delete('{id}', ['uses' => 'TypesController@destroy']);
        });

        $router->group(['prefix' => 'menu'], function() use ($router) {
            $router->get('access', ['uses' => 'MenuController@access']);
            $router->get('select', ['uses' => 'MenUController@select']);
            $router->post('datatables', ['uses' => 'MenuController@datatables']);

            $router->post('', ['uses' => 'MenuController@store']);
            $router->get('{id}', ['uses' => 'MenuController@show']);
            $router->put('{id}', ['uses' => 'MenuController@update']);
            $router->delete('{id}', ['uses' => 'MenuController@destroy']);
        });

        $router->group(['prefix' => 'businesspartner'], function() use ($router) {

            $router->get('select', ['uses' => 'BusinessPartnerController@select']);
        });

        $router->group(['prefix' => 'bpmenu'], function() use ($router) {

            $router->get('load', ['uses' => 'BpMenuController@load']);
            $router->post('save', ['uses' => 'BpMenuController@save']);
        });
    });

    $router->group(['namespace' => 'Settings'], function() use ($router) {

        $router->group(['prefix' => 'role'], function() use ($router) {

            $router->get('access', ['uses' => 'RoleController@access']);
            $router->post('access', ['uses' => 'RoleController@save']);
        });
    });
});
